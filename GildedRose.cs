﻿using System.Collections.Generic;

namespace csharp
{
    public class GildedRose
    {
        #region Privates properties
        private IList<Item> Items;
        private const int maxQuality = 50;
        private const int QualitySulfura = 80;
        #endregion

        #region Constructor
        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
        }
        #endregion

        public void UpdateQuality()
        {
            for (var i = 0; i < Items.Count; i++)
            {
                switch (Items[i].Name)
                {
                    #region AgedBrie
                    case "Aged Brie":
                        {
                            if (Items[i].Quality < 50)
                                Items[i].Quality += 1;

                            goto caseFinal;
                        }
                       
                    #endregion
                    #region Sulfuras
                    case "Sulfuras, Hand of Ragnaros":
                        {
                            Items[i].Quality = QualitySulfura;
                            goto caseFinal;
                        }
                        
                    #endregion
                    #region Backstage passes

                    case "Backstage passes to a TAFKAL80ETC concert":
                        {
                                //Si la date du concert est pasée est passée, la qualité du billet retombe a 0
                                if (Items[i].SellIn <= 0)
                                {
                                    Items[i].Quality = 0;
                                }
                                //Sinon, si la date du concert  est dans moins de 5 jours, 
                                //et que la qualité n'excède pas 50, cette derniere augmente de 3
                                else if (Items[i].SellIn > 0 && Items[i].SellIn <= 5)
                                {
                                    if (Items[i].Quality <= maxQuality - 3)
                                    {
                                        Items[i].Quality += 3;
                                    }
                                    else if (Items[i].Quality <= maxQuality-2)
                                    {
                                        Items[i].Quality +=2;
                                    }
                                    else if(Items[i].Quality < maxQuality)
                                    {
                                        Items[i].Quality++;
                                    }
                                }
                                //Sinon, si la date du concert est dans moins de 10 jours, 
                                //et que la qualité n'excède pas 50, cette derniere augmente de 2
                                else if (Items[i].SellIn > 5 && Items[i].SellIn <= 10 )
                                {
                                    if (Items[i].Quality <= maxQuality - 2)
                                    {
                                        Items[i].Quality += 2;
                                    }
                                    else if (Items[i].Quality < maxQuality)
                                    {
                                        Items[i].Quality++;
                                    }
                                 }
                                //Sinon, la qualité augmente de 1
                                else if (Items[i].Quality < maxQuality)
                                {
                                    Items[i].Quality++;
                                }
                          goto caseFinal;
                        }
                        
                    #endregion
                    #region Cas par default & conjured object 
                    default:
                        {
                            if (Items[i].Name.Contains("Conjured"))
                            {
                                if (Items[i].Quality > 0)
                                {
                                    Items[i].Quality -= 2;
                                }
                            }

                            else if (Items[i].Quality > 0)
                            {
                                Items[i].Quality--;
                            }

                            #region Pour s'ameliorer...
                            /* *-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                             * Pour ameliorer la réutilisabilité du code j'ai pensé à integrer cette partie
                             * de façon a gérér tous les passes backstages et pas 
                             * uniquement le cas du "Backstage passes to a TAFKAL80ETC concert"
                             * Cependant je l'ai commenté car à mon sens cela complique la compréhension du code, 
                             * j'ai donc preferer utiliser le switch case plutot que la foncion contains dans un if
                             *
                             * *-*-*-*-*-*-*-*-*-**--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
                            if (Items[i].Name.Contains("Backstage passes"))
                            {
                               //logique implementée dans le case du dessus
                            }
                            
                            */
                            #endregion
                            goto caseFinal;
                        }

                        
                        //Partie du code s'executant dans tous les cas à la fin de chaque "chemin" du switch case
                        caseFinal:
                        {
                            Items[i].SellIn--;
                            break;
                        }
                        #endregion
                }


            }

         }
    }
}

